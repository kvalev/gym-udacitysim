# gym-udacitysim

This is an OpenAI gym environment for the [Udacity self driving simulator](https://github.com/kvalev/self-driving-car-sim) fork that somewhat supports reinforcement learning.

# Installation

```bash
cd gym-udacitysim
pip install -e .
```

# Usage

```python
import gym
import gym_udacitysim

env = gym.make('UdacitySim-v0')

episodes = 20
steps = env.spec.max_episode_steps # steps per episode

for episode in range(episodes):
    env.reset() # reset the environment before each episode

    for t in range(steps):
        env.render() # (optional) render the environment; currently no-op

        action = env.action_space.sample()  # take a random action
        observation, reward, done, info = env.step(action)
        if done:
            print "Episode finished after {} timesteps".format(t+1)
            break

print "Done"
```