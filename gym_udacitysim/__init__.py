"""This module registers the different environments with the OpenAI gym."""

from gym.envs.registration import register

register(
    id='UdacitySim-v0',
    entry_point='gym_udacitysim.envs:SimEnv',
    local_only=True,
    max_episode_steps=1000 # aka timestep_limit, to be deprecated on 2017/03/01
)
