import base64
from io import BytesIO
from threading import Event, Thread

import numpy as np
import socketio
from gevent import pywsgi
from geventwebsocket.handler import WebSocketHandler
from flask import Flask
from PIL import Image

import gym
from gym import spaces

class SimEnv(gym.Env):
    metadata = {
        'render.modes': ['human']
    }

    def __init__(self):
        # state
        self.on_track = True
        self.steering_angle = 0.0
        self.throttle = 0.2
        self.speed = 0.0
        self.image = None

        # maybe make them configurable
        self.width = 320
        self.height = 160

        # the observation space is the image from the car's front facing camera
        self.observation_space = spaces.Box(low=0, high=255, shape=(self.width * self.height * 3, 1))

        # the action space is a 2d vector, in a continous space [-1, 1]
        # the first element represents the steering angle, the second one - the throttle
        self.action_space = spaces.Box(low=-1, high=1, shape=(2,))

        self.sio = socketio.Server(async_mode='gevent')
        self.sio.on('telemetry', self.on_telemetry)
        self.sio.on('connect', self.on_connect)
        self.sio.on('disconnect', self.on_disconnect)

        # some debug flags
        self.debug_step = 0

        # synchronization primitives
        self.connect_event = Event()
        self.telemetry_event = Event()

        # wrap Flask application with engineio's middleware
        self.app = socketio.Middleware(self.sio, Flask(__name__))

        # start the server, that will handle the communication with the simulator
        self.thread = Thread(target=self._start_server, args=(self.app,))
        #self.thread.daemon = True
        self.thread.start()

    def _step(self, action):
        observation = self._perform_action(action)
        reward = self._calculate_reward()
        done = not self.on_track
        info = {
            'steering_angle': self.steering_angle,
            'throttle': self.throttle,
            'speed': self.speed,
            'step': self.debug_step,
        }

        return observation, reward, done, info

    def _reset(self):
        self.debug_step = 0
        self.sio.emit('reset', data={}, skip_sid=True)

        # now we need to lock and wait for the telemetry update
        # once we get the result we can unlock
        self.telemetry_event.wait()
        self.telemetry_event.clear()

        return self.image

    def _render(self, mode='human', close=False):
        # nothing to do so far
        pass

    def _perform_action(self, action):
        self._send_control(action[0], action[1])

        return self.image

    def _calculate_reward(self):
        if not self.on_track:
            return -1

        # TODO calculate
        return 1

    def _start_server(self, app):
        # deploy as a WSGI server
        # this is a blocking call and should be started in a different thread
        pywsgi.WSGIServer(('', 4567), app, handler_class=WebSocketHandler).serve_forever()

    def _shutdown_server(self):
        exit()

    def _send_control(self, steering_angle, throttle):
        # ensure that we have connected to the simulator first
        self.connect_event.wait()

        print "Sending controls, steering={}, throttle={}".format(steering_angle, throttle)

        self.sio.emit(
            "steer",
            data={
                'steering_angle': steering_angle.__str__(),
                'throttle': throttle.__str__(),
                'i': self.debug_step.__str__()
            },
            skip_sid=True)

        # now we need to lock and wait for the telemetry update
        # once we get the result we can unlock
        self.telemetry_event.wait()
        self.telemetry_event.clear()

    def on_telemetry(self, sid, data):
        print "Received telemetry"

        """
        Event handler for the simulator's telemetry data.
        """
        if not data:
            # NOTE: DON'T EDIT THIS.
            # in a proper autonomous environment this should never be called
            print "Driving manually!"
            self.sio.emit('manual', data={}, skip_sid=True)
            return

        self.on_track = data["ontrack"] == "True"
        self.steering_angle = data["steering_angle"]
        self.throttle = data["throttle"]
        self.speed = data["speed"]
        self.debug_step += 1

        img = Image.open(BytesIO(base64.b64decode(data["image"])))
        self.image = np.asarray(img)

        print (self.steering_angle, self.throttle, self.speed, self.on_track, self.image.size)
        self.telemetry_event.set()

    def on_connect(self, sid, data):
        print "Connected via " + self.sio.transport(sid)

        # now it should be safe to release the connect eventlet
        self.connect_event.set()

    def on_disconnect(self, sid):
        print "Disconnected"
        self._shutdown_server()
