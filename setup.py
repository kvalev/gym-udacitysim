from setuptools import setup

setup(name='gym_udacitysim',
      version='0.0.1',
      install_requires=['gym', 'numpy', 'Pillow', 'python-socketio', 'gevent', 'gevent-websocket']
)
